---
author: Chloe Graham
draft: false
date: 2023-12-01
excerpt: Chloe's painting is inspired by the curious biology of the Crown of Thorns Starfish in the Great Barrier Reef.
featured: true
layout: single
subtitle: ""
tags:
- hugo-site
title: Crown of Stars in the Great Barrier Reef
---


![](painting.png)