---
author: 
cascade:
  show_author_byline: true
  show_comments: false
  show_post_date: true
description: Created by students in the Fall 2023 Principles of Ecology course at LSU.
layout: list-grid
show_author_byline: true
show_button_links: false
show_post_date: false
show_post_thumbnail: true
title: Semester project portfolio
---

** No content for the project index. This file provides front matter for the blog including the layout and boolean options. **
