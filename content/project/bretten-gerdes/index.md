---
author: Bretten Gerdes
draft: false
date: 2023-12-01
excerpt: Bretten's infographic depecits some of the most pressing threats to Louisiana's wetlands.
featured: true
layout: single
subtitle: ""
tags:
- hugo-site
title: Threats to Louisiana's Wetlands
---


![](wetlands.png)