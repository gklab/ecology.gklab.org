---
author: Katie Dinh
draft: false
date: 2023-12-01
excerpt: Katie created a stunning cake inspired by the biology of Ha Long Bay in Vietnam.
featured: true
layout: single
subtitle: ""
tags:
- hugo-site
title: Emerald waters, karst soils
---

Katie's cake represents the geology, biology, and ongoing environmental challenges of Ha Long Bay.

![](featured-hex.jpg)
