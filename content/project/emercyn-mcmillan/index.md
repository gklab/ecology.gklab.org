---
author: Emercyn McMillan
draft: false
date: 2023-12-01
excerpt: Emercyn's websites features the fasinating ecology of the Los Angeles Watershed in southern California.
featured: true
layout: single
subtitle: ""
tags:
- hugo-site
title: Ecology in a dense urban landscape
---

Navigate directly to [Emercyn's website](https://eandemcmillan.wixsite.com/urban-explorations), or explore below.

<iframe id="iframepdf" src="https://eandemcmillan.wixsite.com/urban-explorations" width="1200" height="800"></iframe>

*Featured image of the Angeles National Forest from [Wikipedia](https://en.wikipedia.org/wiki/Angeles_National_Forest#/media/File:Angelesnationalforest.jpg)*
