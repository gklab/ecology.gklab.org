---
author: Dillon Kong
draft: false
date: 2023-12-01
excerpt: Dillon's paintings show the ecological effects that followed the explosions at the nuclear powerplants in Chernobyl.
featured: true
layout: single
subtitle: ""
tags:
- hugo-site
title: Ecological effects of a nuclear disaster
---


<iframe id="iframepdf" src="kong-paintings.pdf" width="1000" height="825"></iframe>