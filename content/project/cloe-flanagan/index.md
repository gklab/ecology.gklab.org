---
author: Cloe Flanagan
draft: false
date: 2023-12-01
excerpt: Cloe was inspired by deep sea ecology to write a series of poems capturing the mystery and environmental challenges of these landscapes. 
featured: true
layout: single
subtitle: ""
tags:
- hugo-site
title: Life in the deep sea
---


### Poem 1

The first poem is from the perspective of a piece of trash that has fallen out of a fishing boat.

> I see the lightest of blues,  
I reach for it right above.  
But it turns the shade of a bruise,  
And my reaching is not enough.  <br><br> 
I wish it ended there,  
I just wanted to go back.  
But now when I look,  
All I see is black.  


### Poem 2

An introduction to the abyssal zone

> It is so quiet,  
Down there in the dark.  
With an atm of 600,  
It's not for the week of heart. <br><br>
Despite the harsh environment,  
There is plenty of life to be found.  
You might not be able to see it,  
But it is all around.   <br><br>
Down here they don’t need light,   
Most creatures can do without.  
But if light is called for,  
Bioluminescence will work no doubt. <br><br>
If you see a light,  
Make sure to be wary.  
It might be an anglerfish,  
And things will get hairy.  <br><br>


### Poem 3 

From the perspective of a grain
of sand, watching as the world around changes faster than it is supposed to.

> I saw the beginning,  
I will see the end.  
But lately I've been thinking,  
And I still can't comprehend. <br> <br>
How is so much damage,  
Done in such a short time?  
And how do humans manage,  
To always make a dime? <br> <br>   
Its though they hate the earth,  
They yearn for its destruction.  
And to make everything worse,  
They fill the oceans with corruption.   <br> <br>
I see new contraptions,  
All of the time.  
Some meant to trap shit,  
And some meant for mining.  <br> <br>
Regardless of function,  
They all do the same.  
Get lost by the humans,  
And then take lives away.   <br> <br>
I remember a time,  
Nature could transcend.  
Before humankind,  
Made it all end.   <br> <br>
Yes, I've seen it all,  
But I'd do it all again.  
Even though I've grown small,  
I don’t want to see it end.  

<br><br>

### Poem 4  

A stray fishing net left behind by a fisherman laments the harm it has inadvertently contributed to.

> I'm sorry I'm so sorry,  
I didn’t want to hurt anybody.  
It’s not my fault I've been left behind,  
And I can't control who gets intertwined. <br><br
If it was up to me,  
I'd set you free.  
But unfortunately,  
Only mankind can make that a reality.  

### Poem 5

The musings of a bone-eating *Osedax* worm

> Food is scarce on the sea floor,  
But when it comes, we feast for months.  
Everyone joins in,  
It causes quite a fuss. <br><br>
First come the scavengers  
Attracted by the flesh  
Sleeper sharks and snow crabs  
Followed by the rest   <br><br>
Once they striped it dry  
They see no further use  
That’s when I come in  
Ready to let loose  <br><br>
They have taken all the meat  
But they left the best part  
They don’t know the bone  
Is where the nutrition is caught  <br><br>
I gather up my acid  
And burrow myself inside  
Consume all the fat I can  
Until my bacteria are satisfied <br><br>
I might not have a mouth  
But my bacteria have me covered  
We are endosymbiotic  
So, with their help I end my hunger  


<br><br>

*Featured photograph of Osedax worms from the [Smithsonian Ocean Life](https://ocean.si.edu/ocean-life/invertebrates/zombie-worms-crave-bone) page.*