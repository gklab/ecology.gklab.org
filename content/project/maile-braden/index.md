---
author: Maile Braden
draft: false
date: 2023-12-01
excerpt: Maile's painting is inspired by the mangrove swamps in Florida.
featured: true
layout: single
subtitle: ""
tags:
- hugo-site
title: Life in mangrove roots
---

<iframe width="900" height="700" src="mangrove-swamp.pdf"></iframe>