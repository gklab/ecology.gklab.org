---
author: Madelynn Nguyen
draft: false
date: 2023-12-01
excerpt: Madelynn created an animated short that recounts the ecological and social consequences of the devastating 2010 Deepwater Horizon oilspill.
featured: true
layout: single
subtitle: ""
tags:
- hugo-site
title: The socioecological consequences of oil spills
---

<iframe width="1000" height="600" src="https://www.youtube.com/embed/nJEl2IjI4PQ?si=mCEJ5qIddAfGj0yg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>