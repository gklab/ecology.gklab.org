---
title: ''
author: "Gaurav Kandlikar"
show_post_date: no
layout: "single-sidebar"
type: "singlepage-TOC"
sidebar_left: yes
---

***Click [here](biol4253-syllabus.pdf) for a PDF version of this syllabus.***

## Welcome to Principles of Ecology!
<div class="row">
  
  <div class="column">
  
My name is Dr. Gaurav Kandlikar (he/him/his), and I am so excited to share this class with you. This is my first year as a professor at LSU, and I'm looking forward to get to know you all, Baton Rouge, and the nature in Louisiana.

My research focuses on plant community ecology, which means I study how plants interact with the environment and with other organisms in their environment. A lot of my work has especially focused on how plants interact with the soil microbiome. I love my job because it has given me the opportunity to visit and learn about amazing places and meet amazing people from all over the world. In my free time I enjoy running/biking, cooking, and exploring new music. 

To contact me about this course, please [send me a message](https://moodle.lsu.edu/user/profile.php?id=108699) through the course moodle. I will generally respond to moodle messages within 24 hours. You can also choose to follow up via email: gkandlikar@lsu.edu (but my response times may be slower).
</div>

<div class="column">
    <img src="GSK_profile.jpg" style="width:800px; padding-top:15px;padding-left:25px;">
</div>
</div>


## Course overview

Ecology as a discipline is motivated by human efforts to describe, understand, predict, and modify nature. Specifically, a lot of work in ecology is interested understanding the various interactions that shape where organisms live and how their abundances change over time. This course will introduce you to the questions that motivate the field of ecology, the approaches that ecologists take to answer these questions, and the implications of ecological research for ongoing environmental challenges. Our goal over the next semester will be to develop skills to:

1.  Describe how the field of ecology tackles the complexity of nature
2.  Explain how mathematical thinking helps generate ecological insights  
3.  Interpret figures and results from published ecological literature  
4.  Discuss the role of ecology in addressing major societal challenges.

The content of the course is broadly organized around different levels of ecological organization. We will begin by addressing how individuals interact with the abiotic environment and with other members of their own species. We will then focus on how different species interact with one another, and how this affects community dynamics. Then, we will learn about the ecology of entire landscapes and ecosystems. At all scales, we will discuss how basic ecological knowledge can help us make sense of -- and deal with -- some of the most pressing ecological challenges that we face today. The course schedule is also designed with substantial in-built flexibility to address your own objectives for the semester, which I will ask you to write about in your first weekly self-reflection essay.


## Course structure and evaluation

The goal of this course is to help you master the fundamentals of ecology, build a framework for how ecological principles relate to ongoing ecological crises, and develop skills that will help you apply fundamental concepts to action.

Traditional grading, which emphasizes one-size-fits-all assignments and high-stakes evaluations, can be an [imperfect system](https://www.chemedx.org/blog/ungrading-what-it-and-why-should-we-use-it?ref=jessestommel.com) for evaluating student progress towards such goals. Thus, rather than reflecting your performance on high-stakes exams and projects, your final grade in this course will reflect your effort and learning in three areas:  

- 70 points (1/3rd of the course total) for [Weekly Reflection](#weekly-reflection) 
- 70 points (1/3rd of the course total) for [Weekly Activities](#weekly-activities) 
- 70 points (1/3rd of the course total) for the [Semester Project](#semester-project) 

Students will be in charge of self-grading the weekly reflection and weekly activities, while grades for the semester project will be decided collaboratively between the student and instructor. **Please expand the boxes in the sections below for more information on how self-grading will work in this class**. We will also devote time during Week 1 to ensure that all students are familiar with this system and understand why this course is adopting this approach.


### Weekly Reflections {#weekly-reflection}

**Each week starting in Week 1**, you will be asked to complete a self-reflection activity in which you take a step back from the content that we are learning in the course, and instead think about how you are growing as a scientist and person. In some weeks, your reflections might primarily focus on what we are learning in class -- for example, is there something we learned that challenges your previous beliefs about how nature works? Are there particular ideas that you find hard to learn, and what are some roadblocks in your way that we could help overcome? In other weeks, a bulk of your reflection may focus on events outside of the course that are impacting your life as a student. 

I will not provide a specific list of questions that I want you to address in each reflection, but I will provide a list of topics each week to help kick-start your own critical thinking process. Note that even when I do provide a list of topics, my intention is for these to just be potential starting points if you are otherwise finding it hard to get started. 

When I engage in thoughtful reflection, I have found that I write about 400 words on a given topic - so I encourage your reflections to be somewhere around that benchmark. But please note that 400 words is neither a 'minimum' nor a 'maximum' word count; I am suggesting it just to help you know how much effort I am hoping you put into this activity.  More details are available in the box below (click to expand).

<div class="div-details">
<div class="div-indent">
<details>
<summary>Click here for more details about Weekly Reflections
</summary>

#### Why are we doing this?

The goal for this activity is to help us develop a practice of critical self-reflection.[^1] Such self-reflection, in which we regularly dedicate time for setting goals, monitor and reflect on our growth, and use previous experiences to shape our future, is a key ingredient in [meta-cognition](https://cft.vanderbilt.edu/guides-sub-pages/metacognition/), and can help foster long-term and sustained learning. In addition to helping you reflect on your learning, your weekly reflections will also help me better understand how the course is progressing, and help identify ways for the class to be more inclusive and impactful for all students.

A second motivation for this activity is to encourage you to think freely and broadly about how the things we learn in class interact with your lives outside the class. For example, I encourage you to use these self-reflections as a space to think about how the topics we cover in class relate to societal challenges . Equally importantly, I also encourage you to reflect on how the field of ecology itself can learn, grow, and change based on what is happening in the world.

#### Who will read my self-reflection?

You should consider yourself as the primary audience for your self-reflection. However, you should not consider this as an entirely private venue: I plan to regularly read students' self-reflection essays to get a sense of how things are progressing, and to identify areas of potential improvement/adjustment in the course schedule. 

#### How are weekly reflections graded? {#weekly-reflection-grading}

Weekly reflections are meant as a personal endeavor that center your own learning and growth. Thus, you will be in charge of grading your own weekly reflections. Specifically, every week starting in Week 2, you will be asked to return to re-read your previous week's reflection and self-assign a grade out of 5 points. 

#### How should I grade my self-reflection? {#how-to-grade-reflection}

There is no set format for what makes for a good reflection essay. Reflections can be retrospective (thinking back over the past) or prospective (thinking ahead to your future); narrow or broad; long or short; hopeful or pessimistic; vague or specific. What defines a good reflection is whether it best captures your state of mind and helps you grow as a learner. While I will not be reading your essays with an eye towards grading, I reserve the right to request a one-on-one discussion if there are consistent disparities between how I would have graded your essay and your self-assigned score.


[^1]: I think of critical self-reflection as the act of consciously thinking about our past, present, and future selves to ensure that we are getting the most out of our lives

</details>
</div>
</div>


### Weekly Activities {#weekly-activities}

**Each week starting in Week 2**, you will be asked to complete and upload an activity sheet related to course content. Activities vary from week-to-week, but typically involve involve some combination of engaging with primary ecological literature, engaging with quantitative topics we cover in class, and/or engaging with ecological ideas through other mediums like podcasts or news stories. **All activities should be uploaded to our course website by Thursday night**, as most Friday lecture sessions will be structured around discussing and reviewing the material on that week's activity sheet.


<div class="div-details">
<div class="div-indent">
<details>
<summary>Click here for more details about the Weekly Activities
</summary>


#### How much time should I expect each Weekly Activity to take?

In most weeks, I expect the Weekly Activity to take 3-4 hours to complete. In some weeks, you will be asked to share progress on the [semester project](#semester-project) in addition to completing the weekly assignment; in such weeks, the weekly assignments will be lighter. If you are finding that the Weekly Activities are taking you considerably more time to complete than these guidelines, please let me know. 

#### How will Weekly Activities be graded?

The goal for Weekly Activities is to help you deeply engage with course content and evaluate your own learning of ecology. Thus, as for the [Weekly Reflection](#weekly-reflection), you will be in charge assigning a grade to your own Weekly Activity submissions. Specifically, every week starting at the end of Week 2, you will be asked to return to the previous week's activity and self-assign a grade out of 5 points. 

#### How should I grade my Weekly Activity? 

To help you grade your own work, I will provide you with a completed version of the weekly activity that I would consider as an exemplar  submission. However, your submission does not have to mirror my 'exemplar' to earn full points. As you grade your own work, I encourage you to consider a variety of factors, including your own perceived level of engagement with the ideas, how much time you spent working on the activity, how deeply you engaged with your classmates in discussing the activity during the Friday lecture, how much you feel you have advanced your understanding as a result of completing the activity, and any other factor you think relevant. I will read through your submissions to check for general trends in understanding/interpretation. If I feel that your self-assigned grades consistently differ substantially from how I might have approached the evaluation, I may request an individual meeting to discuss grades. 

</details>
</div>
</div>

### Semester project {#semester-project}

Over the next few months, you will work towards a semester project in which you get to know and describe the ecology of an area of your choosing. The format of the project can be of your choosing - e.g. you may choose to make an infographic, record a podcast episode, write and illustrate a children's book, compose a new song. (This list is not meant to be comprehensive! I encourage you to use this as an opportunity to develop other media/communication skills you are interested in.)

We will discuss details of the semester project in class during the second week of class. 

### Late submissions  

Given the rapid pace of the semester, I expect students to complete all submissions according to the stated due-dates. Each weekly reflection comes with a 24-hour grace period; weekly activities do not include such a blanket grace period. Over the semester, each student is allowed a "free pass" on one weekly reflection, and one weekly activity - no questions asked.  Beyond the grace periods and free pass, if an illness or other life event delays your submission of a reflection or activity, please talk to me in person and/or over moodle. 

I expect all assignments related to the semester project to be submitted on time. As above, if an illness or other life event delays your submission, please talk to me in person and/or over moodle.

### Weekly coworking sessions

I love meeting with students outside of class hours to work through course content, to explore ideas that extend beyond what we have to discuss in the classroom, or chat about anything else on your mind. I will be available for group co-working sessions for this course from 10.30am-11.30am in the Student Union Center (exact location TBA). I am also available for individual meetings on Wednesday afternoons after lecture, from 3.30-5pm on Zoom (link available in the [course Moodle](https://moodle.lsu.edu/course/view.php?id=38672#section-0)). Please message me on Moodle or email me to set up an appointment if this time does not work for you. 

### Letter grades

At the end of the term, letter grades will be assigned as follows:
```
    A+ for earning 206-210 points over the semester;    
    A  for earning 195-205 points over the semester;  
    A- for earning 189-194 points over the semester;   
    B+ for earning 183-188 points over the semester;   
    B  for earning 174-182 points over the semester;  
    B- for earning 168-173 points over the semester;   
    C+ for earning 162-167 points over the semester;   
    C  for earning 153-161 points over the semester;  
    C- for earning 147-152 points over the semester;   
    D+ for earning 140-146 points over the semester;  
    D  for earning 132-139 points over the semester;  
    D- for earning 126-131 points over the semester;  
    and F for earning fewer than 126 points over the semester. 
```

## Calendar

***Daily schedule may change. Please check this website for the most recent version!***


| Week/Date | Topic | Activities | Self reflection | Semester project |
| ---------  | ----- | ---------- | --------------- | ---------------- |
| Week 1 <br> (21 Aug)| What is ecology, and how is it done? | [Week 1](../weekly-activities/week-01/) | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1952086) | N/A |
| Week 2 <br> (28 Aug)   | Population Ecology pt. 1 <br> How populations grow (or shrink) | [Link to paper](/pdf/Miriti_JoE_2006.pdf); [Link to activity sheet](/pdf/miriti-paper-questions.pdf)| [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976689) | Overview of semester project presented in class |
| Week 3 <br> (4 Sept) <br> *No Monday class*  | Population Ecology pt. 2 <br> Stage-structured populations | [Link to paper](/pdf/Crouse_Ecology_1987.pdf); [Link to activity sheet](/pdf/Week-3-activity-questions.pdf) | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976690) | N/A |
| Week 4 <br> (11 Sept)  | Population Ecology pt. 3: Metapopulation dynamics | [Link to activity](/pdf/Week-4-activity-questions.pdf) | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976691) | [List of potential focal communities](/pdf/potential-communities.pdf) due |
| Week 5 <br> (18 Sept)   | Community Ecology pt. 1  -  interactions within guilds 1 | [Link to activity](/pdf/Week-5-activity-questions.pdf) | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976692) | [List of potential media/formats](/pdf/potential-formats.pdf) due |
| Week 6 <br> (25 Sept)   | Community Ecology pt. 2  <br> interactions within guilds 2 | [Link to activity](/pdf/Week-6-activity-questions.pdf) | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976693) | N/A |
| Week 7 <br> (2 Oct) <br> *No Friday class*    | Community Ecology pt. 3 <br> interactions between guilds | [Link to activity](/pdf/Week-7-activity-questions.pdf) | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976694) | N/A |
| Week 8 <br> (9 Oct)   | Community Ecology pt. 4  <br> interactions between guilds | *tba* | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976854) | [List of potential papers for annotated bibliography due](/pdf/semester-project-annotated-bibliography.pdf) |
| Week 9 <br> (16 Oct)   | Mid-semester check-in <br> *Opportunity for schedule adjustment and student feedback* | [Link](/pdf/Week-9-activity-questions.pdf) | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976855) | N/A |
| Week 10 <br> (23 Oct)   | Landscape Ecology: Quantifying biodiversity | *tba* | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976856) | N/A |
| Week 11 <br> (30 Oct)   | Ecosystem Ecology pt. 1 <br> Nutrient cycling and ecosystem productivity | *tba* | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976857) | N/A |
| Week 12 <br> (6 Nov)   | Ecosystem Ecology  pt. 2 <br> | *tba* | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976858) | [Formal proposal due](/pdf/semester-project-formal-proposal.pdf) |
| Week 13 <br> (13 Nov)   | Ecology and Me  pt. 1 <br> Urban ecology| *tba* | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976859) | N/A |
| Week 14 <br> (20 Nov)  <br> *No W/F class*   | *Thanksgiving week*                  | *tba* | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976861) | Project check-in with Gaurav (Monday in class or Tuesday on campus) |
| Week 15 <br> (27 Nov)   | Ecology and Me pt. 2 <br> How ecology matters to human health and wellbeing | *tba* | [Link](https://moodle.lsu.edu/mod/assign/view.php?id=1976862 ) | N/A |
| Week 16 <br> (Dec 4)   | Allotted Finals time - Tues. Dec 5 at 5.30pm | None | None | Semester project round-robin |


## Course Context

### Student health and wellness

Your mental and physical health are important to me. If you find yourself struggling with your mental or physical health this semester, please feel free to approach me and I will do my best to flexible and accommodating with the class, and to help connect you to professional support on campus. You can also find support for both mental and physical health concerns through LSU's [Student Health Center](https://www.lsu.edu/shc/mental-health/mhshome.php). 

### Accessibility statement

My goal is to help you learn and achieve your goals in this class. If you have any mental or physical health difficulties that might affect your engagement with the course, please contact me as soon as you can. I also encourage you to see a [staff member in Disability Services](https://www.lsu.edu/disability/about/staff.php), who can help explore more formal course accommodations.

### Diversity, Equity & Inclusion our classroom

*Adapted from the [LSU Diversity Statement](https://lsu.edu/diversity-statement/index.php)*  

We believe diversity, equity, and inclusion enrich the educational experience of our students, faculty, and staff, and are necessary to prepare all people to thrive personally and professionally in a global society. Therefore, we are firmly committed to an environment that affords respect to all members of our community. We will work to eliminate barriers that any members of our community experience.

To make our community a place where that can happen, we must recognize and reflect on the inglorious aspects of our history. Throughout this course, we will acknowledge the need to confront the ways racism, sexism, ableism, ageism, classism, LGBTQ+ phobia, intolerance based on religion or on national origin, and all forms of bias and exploitation have shaped our everyday lives.

### Academic integrity

*Adapted from [UC Berkeley Center for Teaching and Learning](https://teaching.berkeley.edu/statements-course-policies)*

You are a member of an academic community at one of the world’s leading research universities, and I hold all students in this class up to the high standards of academic integrity this entails. Universities like LSU create knowledge that has a lasting impact in the world of ideas and on the lives of others; such knowledge can come from an undergraduate project as well as the lab of an internationally known professor.  One of the most important values of an academic community is the balance between the free flow of ideas and the respect for the intellectual property of others. Researchers don't use one another's research without permission; scholars and students always use proper citations; professors may not circulate or publish student papers without the writer's permission; and students may not circulate or post materials (handouts, exams, syllabi--any class materials) from their classes without the written permission of the instructor. These same principles apply to work generated from Artificial Intelligence tools like ChatGPT: I acknowledge these as potentially useful tools to help us learn and generate knowledge about our world, but I ask that you cite its use when you do use it, and that you don't abandon your own creativity and caution when you incorporate its outputs. 

Any material submitted by you and that bears your name is presumed to be your own original work that has not previously been submitted for credit in another course unless you obtain prior written approval to do so from your instructor. In all of your assignments, including your homework or drafts of papers, you may use words or ideas written by other individuals in publications, web sites, or other sources, but only with proper attribution. If you are not clear about the expectations for completing an assignment or taking a test or examination, be sure to seek clarification from your instructor beforehand. Finally, you should keep in mind that as a member of the campus community, you are expected to demonstrate integrity in all of your academic endeavors and will be evaluated on your own merits. *The consequences of cheating and academic dishonesty—including a formal discipline file, possible loss of future internship, scholarship, or employment opportunities, and denial of admission to graduate school—are simply not worth it*.

*Thanks to LSU [Archival Education and Research Institute](https://www.lsu.edu/chse/sis/resources/aeri/) as the source of these statements* 

### Land Acknowledgement Statement

We would like to acknowledge the indigenous history of Baton Rouge, and more broadly, Louisiana as part of our responsibility to acknowledge, honor, and affirm indigenous culture, history and experiences. We recognize the communities native to this region including the Caddo Adai Indians of Louisiana, Biloxi Chitimacha Confederation, Chitimacha Tribe of Louisiana, Choctaw Nation, Coushatta Tribe, Four Winds Cherokee Tribe, Muscogee (Creek), Point au Chien Tribe, Tunica Biloxi Tribe, United Houma Nation, and others whose memories may have been erased by violence, displacement, migration, and settlement. We thank them for their strength and resilience as stewards of this land and are committed to creating and maintaining a living and learning environment that embraces individual differences, including the indigenous peoples of our region.

For more information on the indigenous peoples of Louisiana, please see the [LSU Libraries Lib Guide](https://guides.lib.lsu.edu/c.php?g=1095533&p=7990126) or one of the links at [this site](https://www.lsu.edu/chse/sis/resources/aeri/acknowledgement.php). To identify the indigenous communities in your local area, please see the [Native Land Map](https://native-land.ca/).

### Labor Acknowledgement Statement

We would like to acknowledge that much of the culture, economic growth, and development of Baton Rouge, Louisiana State University and Louisiana as a whole, was built on the labor of enslaved people, primarily of African descent, who were kidnapped and brought to Louisiana into chattel slavery. We wish to acknowledge their labor and honor their lives and legacy, and that of their descendants, who suffered continued harm as the result of Jim Crow and other forms of entrenched white supremacy that continue today. Although technically ending slavery following the adoption of the 13th Amendment to the U.S. Constitution, we recognize its continued impact felt by those forced to work by violence, threats, and coercion.  

Louisiana State University sits on three former plantations: Arlington, Nestle Down, and Gartness. According to the LSU student public history project [Slavery in Baton Rouge](https://slaverybr.org/the-former-plantations-of-lsus-campus/):

> *What is now the central campus of LSU was once the location Gartness Plantation’s cabins, storehouses, and master’s house. However, the site where Arlington Plantation’s house once stood has since been eroded by the Mississippi. Gartness Plantation’s property lines closely line up with central campus. The Arlington Plantation starts near LSU’s Vet School and ends at what Brightsideightside Drive. The Gartness plantation, Magnolia Mound, D. Daigre, and J. H. Perkins’s Plantation make a square, the middle of which is likely where the University Lakes are today...The lives of the enslaved people who once lived on Arlington, Nestle Downs, and the Gartness Plantations should not be forgotten. It is important to identify that plantation slavery was prevalent in the area during the Antebellum Era and that these plantations had an impact that can still be seen throughout modern Baton Rouge. Names such as Arlington and Gourrir are still used throughout the area in the locations where the former properties of these plantations existed.*

(Adapted from the work of [Dr. TJ Stewart](https://www.diverseeducation.com/demographics/african-american/article/15108677/on-labor-acknowledgements-and-honoring-the-sacrifice-of-black-americans) & [Solid Ground](https://www.solid-ground.org/))

Thanks to LSU [Archival Education and Research Institute](https://www.lsu.edu/chse/sis/resources/aeri/) as the source of these statements