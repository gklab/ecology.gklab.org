---
description: Land and Labor Acknowledgement Statements
draft: false
layout: standard
show_title_as_headline: true
---

*Thanks to LSU [Archival Education and Research Institute](https://www.lsu.edu/chse/sis/resources/aeri/) as the source of these statements* 

# Land Acknowledgement Statement

We would like to acknowledge the indigenous history of Baton Rouge, and more broadly, Louisiana as part of our responsibility to acknowledge, honor, and affirm indigenous culture, history and experiences. We recognize the communities native to this region including the Caddo Adai Indians of Louisiana, Biloxi Chitimacha Confederation, Chitimacha Tribe of Louisiana, Choctaw Nation, Coushatta Tribe, Four Winds Cherokee Tribe, Muscogee (Creek), Point au Chien Tribe, Tunica Biloxi Tribe, United Houma Nation, and others whose memories may have been erased by violence, displacement, migration, and settlement. We thank them for their strength and resilience as stewards of this land and are committed to creating and maintaining a living and learning environment that embraces individual differences, including the indigenous peoples of our region.

For more information on the indigenous peoples of Louisiana, please see the [LSU Libraries Lib Guide](https://guides.lib.lsu.edu/c.php?g=1095533&p=7990126) or one of the links at [this site](https://www.lsu.edu/chse/sis/resources/aeri/acknowledgement.php). To identify the indigenous communities in your local area, please see the [Native Land Map](https://native-land.ca/).

# Labor Acknowledgement Statement

We would like to acknowledge that much of the culture, economic growth, and development of Baton Rouge, Louisiana State University and Louisiana as a whole, was built on the labor of enslaved people, primarily of African descent, who were kidnapped and brought to Louisiana into chattel slavery. We wish to acknowledge their labor and honor their lives and legacy, and that of their descendants, who suffered continued harm as the result of Jim Crow and other forms of entrenched white supremacy that continue today. Although technically ending slavery following the adoption of the 13th Amendment to the U.S. Constitution, we recognize its continued impact felt by those forced to work by violence, threats, and coercion.  

Louisiana State University sits on three former plantations: Arlington, Nestle Down, and Gartness. According to the LSU student public history project [Slavery in Baton Rouge](https://slaverybr.org/the-former-plantations-of-lsus-campus/):

> *What is now the central campus of LSU was once the location Gartness Plantation’s cabins, storehouses, and master’s house. However, the site where Arlington Plantation’s house once stood has since been eroded by the Mississippi. Gartness Plantation’s property lines closely line up with central campus. The Arlington Plantation starts near LSU’s Vet School and ends at what Brightsideightside Drive. The Gartness plantation, Magnolia Mound, D. Daigre, and J. H. Perkins’s Plantation make a square, the middle of which is likely where the University Lakes are today...The lives of the enslaved people who once lived on Arlington, Nestle Downs, and the Gartness Plantations should not be forgotten. It is important to identify that plantation slavery was prevalent in the area during the Antebellum Era and that these plantations had an impact that can still be seen throughout modern Baton Rouge. Names such as Arlington and Gourrir are still used throughout the area in the locations where the former properties of these plantations existed.*

(Adapted from the work of [Dr. TJ Stewart](https://www.diverseeducation.com/demographics/african-american/article/15108677/on-labor-acknowledgements-and-honoring-the-sacrifice-of-black-americans) & [Solid Ground](https://www.solid-ground.org/))