---
action_label: Read the syllabus &rarr;
action_link: /syllabus
action_type: text
description: Welcome to Principles of Ecology! I am thrilled that you are here, and I am excited to share this class with you over the semester. <br> <br>
  Please navigate to the  [syllabus](syllabus) for an overview of our semester, or head directly to the [course materials](weekly-activities)
.image_left: false
images:
- img/cityscape.jpg
show_action_link: true
show_social_links: false
text_align_left: false
title: Principles of Ecology
subtitle: Biol 4253 at LSU, Fall 2023, with Dr. Gaurav Kandlikar
type: home
---

** index doesn't contain a body, just front matter above.
See index.html in the layouts folder **
