---
title: ""
format: 
  pdf:
    output-file: "potential-formats"
geometry: margin=2cm
---


## Semester Project - List of potential formats

As you now know, the format for this course's Semester Project is an "*UnEssay*", in which the goal is for you to explore+communicate ecology and environmental challenges of your focal ecological community in any format you wish. Just as some examples, you might decide to write a [children's book](https://www.teachingexpertise.com/classroom-ideas/rainforest-books-for-kids/), design an [infographic](https://www.npr.org/2023/09/05/1196976849/stomp-scrape-repeat-what-you-can-do-to-stop-the-spotted-lanternfly), record a [podcast episode](https://www.wwno.org/podcast/sea-change), or [compose music](https://www.youtube.com/watch?v=5t08CLczdK4). Part of the goal for the Semester Project is to develop creative thinking and persistence - skills that are essential for tackling contemporary ecological challenges. 

**The goal for *this assignment*** is to help you think through potential formats for your project in a structured way. (Note that in this assignment, "format" and "medium" are both used to mean the type of project you are developing - video, podcast, story, etc). 

## Please address the following questions. 

**1.** What kind of project do you hope to develop? What draws you towards this medium? 

**2.** Address one of the following:  
**2a.** If you have previously worked in this medium, what are some of its strengths that you think make it well suited for this project? What is some aspect of this medium that you have never explored before, that you think might be worth exploring this semester? Remember that part of the goal is to help you push your creative limits.  
**2b.** If you have never worked in this medium before, why do you think it is well suited for your project? What are some aspects of working in this medium that you are most excitd to learn about this semester? 

**3.** Using your favorite search engine, identify and engage with (i.e. read, listen to, etc.) 2-4 examples of your chosen format that you might find inspiring as you work on your project. For example, if I was planning to record a podcast episode, I might listen to an episode of the [Sea Change Podcast](https://www.wwno.org/podcast/sea-change) as a potential resource. What did you like about this application that you might want to pull into your own work?  

*If you are unable to find 2-4 examples of your chosen format*, try to get as close as possible - and if you still don't find any, write about why you think your chosen medium isn't well-represented. 

**4.** What do you think will be the main challenges in putting together your project? Do you have concerns about accessing technology to help achieve your project? Are there resources on the LSU campus that might help you achieve your project? E.g. LSU's [Studio 151](https://www.lsu.edu/hss/scrn/studio_151.php) can give access to 3D printers, recording studios, etc.

**5.** What is your back-up plan in case the first choice medium does not work out? 