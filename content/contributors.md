---
description: Thank you to all the contributors!
draft: false
layout: standard
show_title_as_headline: true
title: Contributors
---

Thank you to all the folks who have contributed both technical and creative skills to this project:

+ [Agnieszka Stankiewicz](https://unsplash.com/@dubai_love_story?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) for providing the lecture page landing page photo on [Unsplash](https://unsplash.com/photos/a-bunch-of-bananas-are-growing-on-a-tree-aWw04QNAhwk?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)

+ [Khara Woods](https://unsplash.com/@kharaoke?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) for providing the site's landing page photo on [Unsplash](https://unsplash.com/photos/vInzROa1rVQ?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
  

+ [Desirée De Leon :giraffe:](http://desiree.rbind.io/) (designed 5 of the custom color themes, made illustrations for the workshop, and provided general aesthetic feedback along the way)

+ [Garrick Aden-Buie :mage:](https://www.garrickadenbuie.com/) (debugged headroom.js and lent his panelset.js code to the theme)

+ [Allison Horst :dog2:](https://www.allisonhorst.com/) (awesome illustrations of campfires, seedlings, and evergreens, as well as my R Markdown hedgehog mascot :hedgehog:)

+ [Maëlle Salmon :fishing_pole_and_fish:](https://masalmon.eu/) (help with features, and naming the theme [Hugo Apéro](https://hugo-apero.netlify.app/)!)

+ [Christophe Dervieux :crayon:](http://cderv.rbind.io/) (thinking through blogdown/Hugo intricacies and syntax highlighting)

+ [Yihui Xie :martial_arts_uniform:](https://yihui.org/) (for the blogdown package, getting me hooked on Hugo, and helping me with layout code inspired by his many Hugo themes)

+ [Athanasia Monika Mowinckel :purple_heart:](https://drmowinckels.io/) (for help finding :bug: and SASS support for making color themes work so much better :art:)

+ [Jannik Buhr :otter:](https://jmbuhr.de) (enabling math rendering with mathjax and katex)

And last but not least, Eric Anderson and the team at [Formspree](https://formspree.io/) for developing a Hugo theme with such great bones: <https://github.com/formspree/blogophonic-hugo>
