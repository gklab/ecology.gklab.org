---
author: Alison Hill
type: collection
cascade:
  layout: single-series
  sidebar:
    description: "Here you will find each week's lectures, in-class and out-of-class activities, and links to weekly submissions on the course Moodle."
    show_author_byline: true
    show_post_date: true
    show_sidebar_adunit: false
    text_contents_label: This week
    text_link_label: ""
    text_link_url: ""
    text_series_label: Semester overview
    title: Weekly activities
description: "Here you will find each week's lectures, in-class and out-of-class activities, and links to weekly submissions on the course Moodle"
layout: list-sidebar
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
thumbnail_left: false
title: Weekly activities
---
