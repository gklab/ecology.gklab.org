---
title: ""
format: 
  pdf:
    output-file: "week-10-activity"
geometry: bmargin=2cm

---


\vspace{-5em}

## Week 10 Activity

Read the paper "[Fifteen forms of biodiversity trend in the Anthropocene](https://doi.org/10.1016/j.tree.2014.11.006)" (PDF available on Moodle and course website), and answer the following questions.

1. In your own words, define $\alpha$ diversity and $\beta$ diversity. (0.5pt)

2. What would it mean if a local community has a neutral trend in temporal $\alpha$ diversity (**T$\alpha$-L**, in the paper's framework), but high levels of temporal $\beta$ diversity (**T$\beta$-L**)? (0.5pt)

3. The paper identifies several human activities that contribute to decreasing spatial $\beta$ diversity at the meta-community and biogeographic scales. In your own words, define spatial $\beta$ diversity, and name the two human activities that the authors list as affecting this dimension of biodiversity. (1pt)

4. Box 4 of the paper presents outstanding questions for the field. Pick one of the themes under the "Research questions" or "Policy actions" headings that stood out to you as especially important areas that you would be excited to see develop. Why do you think advances in this area would help advance the field? Regardless of your future career plans, what is one way that you can see yourself contributing to the future of biodiversity science? (1pt)


5. One of the themes from this paper is that unpacking news headlines about biodiversity loss reveals a lot of complexity and nuance about what is going on around the world in terms of biodiversity dynamics. How important do you think it is for scientists to convey this nuance to the public? In other words, do you agree with the authors that integrating nuance will "strengthen rather than weaken the position and credibility of biodiversity science in the policy arena and engender public engagement as we more accurately describe the changes everyone is observing in biodiversity in the Anthropocene"? Explain your thinking. (1 pt)


6. Listen to this podcast episode with Dr. Anne Magurran, one of the co-authors of this paper, on the BBC show "The Life Scientific": https://www.bbc.co.uk/sounds/play/w3ct0t18. (You can also find this by search for "Anne Magurran" and/or The Life Scientific on Apple Podcasts, Spotify, etc.). What is something about Dr. Magurran's career journey or about her work that you find intriguing? (1 pt)

