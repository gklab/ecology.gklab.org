---
date: "2021-01-01"
draft: false
type: collection
excerpt: Measuring biodiversity
subtitle: ""
title: Week 10
weight: 10
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week10/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976856
- icon: book
  icon_pack: fa
  name: Weekly activity
  url: pdf/week-10-activity.pdf
---

## Overview

This week, we will learn about the surprisingly tricky task of quantifying biodiversity and biodiversity change. For the Weekly Activity, students will read a [review paper](/pdf/McGill-2015-biodiversityTrends.pdf) about biodiversity dynamics and complete an associated [activity sheet](/pdf/week-10-activity.pdf).


Cover photo of an American Avocet from [AllAboutBirds](https://www.allaboutbirds.org/guide/American_Avocet/id).