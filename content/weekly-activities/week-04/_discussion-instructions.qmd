---
title: ""
format: 
  pdf:
    output-file: "structured-discussion-questions"
geometry: margin=2cm
---

## Structured group discussion

- Groups of 4 people each, discussion for ~25 minutes.

- Nominate one person as *facilitator*, one as *recorder*, one as *reporter*

- <mark>***Facilitator***</mark>: Manage group discussion time (~5 minutes per person); ensure that everyone gets a chance to speak/ask questions; encourage conversation if things get quiet

- <mark>***Recorder***</mark>: Document the conversations and share notes with relevant individuals (e.g. if we are discussing my idea, Recorder would keep track of the questions that come up and share that with me after the group discussion)

- <mark>***Reporter***</mark>: Share group's ideas with the class - what is each person's top choice for ecological community; what are some challenges/questions that arose; what are next steps, etc.

- <mark>Roles switch next week</mark>

## Structured group discussion

In your groups, please discuss

- What idea(s) you had going into this assignment, and why?

- From your literature search, do you feel that your idea was too narrow/broad/just-right/other?

- Among the primary literature you are finding (peer-reviewed articles in formal scientific journals), is there a good mix of "basic" ecology and more "applied" environmental/conservation/social science? Or is there a bias towards one or the other?

- From your searches so far, do you see ways to connect material from class to your semester project? (either material so far, or from future topics)

- What was one paper/idea/resource that surprised you from your search? 

- What do you think are your next steps for either (a) selecting a focal community from among your options, (b) or getting more information on the community you have decided to focus on?

