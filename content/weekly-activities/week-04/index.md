---
date: "2021-01-01"
draft: false
type: collection
excerpt: Population Ecology, part 3
subtitle: ""
title: Week 04
weight: 4
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week4/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976691
- icon: book
  icon_pack: fa
  name: Weekly Assignment
  url:  https://moodle.lsu.edu/mod/assign/view.php?id=2029563
- icon: hippo
  icon_pack: fa
  name: Semester Project update
  url:  https://moodle.lsu.edu/mod/assign/view.php?id=2029736
---

## Overview

In lecture this week, we will learn how to incorporate in biological reality in the form of spatial structure. We will begin with an overview of a simple model of meta-population dynamics, in which we track the occupancy of different connected patches on a landscape. We will go over the empirical applications of meta-population thinking, including its relevance to models of disease spread. 

The [Weekly Activity](/pdf/Week-4-activity-questions.pdf) is a review of some of the concepts we have covered in class so far, including a case study about the invasion of Burmese Pythons in the Florida Everglades. This week, students are also asked to generate a list of potential communities to focus on for the [semester project](/semester-project). Details of that activity are available at [this link](/pdf/potential-communities.pdf). 

We will also spend some time this week focused on the semester project - the goal for this week is to [explore potential ecological communities](/pdf/potential-communities.pdf) that students can work on. We will end the week with in-class small group discussions about the [semester project communities](/pdf/semester-project-community-discussion.pdf)

This week's cover image is from the [Red Cockaded Woodpecker](https://www.allaboutbirds.org/guide/Red-cockaded_Woodpecker/photo-gallery/297701441) page on AllAboutBirds. 