---
title: "Community Ecology, part 2"
subtitle: "Principles of Ecology\nWeek 7"
format: 
  revealjs:
    theme: [moon, ../../../static/custom.scss]
    incremental: true   
    hash-type: number
    output-file: "slides"
    chalkboard: true
    fig-width: 6
from: markdown+emoji
---

```{r setup, include = FALSE}
library(tidyverse)
library(ecoevoapps)
knitr::opts_chunk$set(echo = FALSE, fig.align = 'center')
```

## Logistics

- No class on Friday, so this week's activity is due on Sunday night
- This week's activity asks you to spend some time outside, connecting with nature. 
  - I am planning to go on walks near campus/in town - let me know if you would like to join. 

## Logistics

- Looking ahead: Semester project Annotated Bibliography due at the end of **Next** week. 
  - Details on Moodle/Course website, will cover in class on Wednesday
  
## This week

1. Wrap up Lotka-Volterra competition model
2. How insights from simple models of competition extend to community dynamics more broadly

## Lotka-Volterra wrap-up

:::{.fragment}
By conducting null-cline analyses, we identified several possible outcomes of what happens when species interact: 
:::
  
- Species $1$ wins, species $2$ disappears from the system
- Species $2$ wins, species $1$ disappears from the system
- Both species persist at equilibrium
- The outcome of competition depends on initial conditions

:::{.fragment}
<mark>***When do these conditions arise?***</mark>
:::

## 

```{r, warning=F, fig.width=10}
params_e1 <- c(r1 = 0.1, r2 = 0.1, a11 = 0.001, a12 = 0.0005, a21 = 0.0012, a22 = 0.002)
params_e2 <- c(r1 = 0.1, r2 = 0.1, a11 = 0.001, a12 = 0.0005, a21 = 0.0002, a22 = 0.0002)
params_co <- c(r1 = 0.1, r2 = 0.1, a11 = 0.001, a12 = 0.0005, a21 = 0.0005, a22 = 0.001)
params_pe <- c(r1 = 0.1, r2 = 0.1, a11 = 0.0015, a12 = 0.0012, a21 = 0.0018, a22 = 0.001)

simdf_e1 <- run_lvcomp_model(params = params_e1)
simdf_e2 <- run_lvcomp_model(params = params_e2)
simdf_co <- run_lvcomp_model(params = params_co)
simdf_pe <- run_lvcomp_model(time = 1000, params = params_pe, init = c(N1 = 200, N2 = 200)) 
simdf_p2 <- data.frame(run_lvcomp_model(time = 1000, params = params_pe, init = c(N1 = 100, N2 = 300)) )

library(patchwork)

plot_e1 <- plot_lvcomp_portrait(simdf_e1, params_e1)  
plot_e2 <- plot_lvcomp_portrait(simdf_e2, params_e2)  
plot_co <- plot_lvcomp_portrait(simdf_co, params_co) 
plot_pe <- plot_lvcomp_portrait(simdf_pe, params_pe) +
    geom_path(inherit.aes = F, data = simdf_p2, aes(x = N1, y = N2), linewidth = 1.2) +
    geom_point(inherit.aes = F, data = simdf_p2[1,], aes(x = N1, y = N2), size = 3)
    
plot_e1 + plot_e2 + plot_co + plot_pe +   
plot_layout(ncol = 2, guides = "collect")
```
##

<mark>***Conditions for exclusion***</mark>

```{r fig.height = 3}
{plot_e1 + 
    annotate("text", y = Inf, x = 500, hjust = 0, vjust = 1, label = "a11 < a21\na12 < a22")} + 
  
  {plot_e2 + 
       annotate("text", y = Inf, x = 500, hjust = 0, vjust = 1, label = "a22 < a12\na21 < a11")} +
plot_layout(ncol = 2, guides = "collect")
```

::: {.fragment}
<mark>***One species is substantially more sensitive to inter- and intra-specific competition than the other species***</mark><br>
(More sensitive species driven to exclusion)
:::

## 

<mark>***Conditions for coexistence***</mark>

```{r fig.height = 3}
{plot_co + 
    annotate("text", y = Inf, x = 500, hjust = 0, vjust = 1, 
             label = "a11 > a21\na22 > a12")}
```

::: {.fragment}
<mark>***Each species exerts stronger competitive effects on conspecifics than on heterspecifics***</mark>
:::

## 

<mark>***Conditions for priority effects***</mark>

```{r fig.height = 3}
{plot_pe + 
    annotate("text", y = Inf, x = 500, hjust = 0, vjust = 1, 
             label = "a11 < a21\na22 < a21")}
```

::: {.fragment}
<mark>***Each species exerts stronger competitive effects on heterospecifics than on conspecifics***</mark>
:::

## {.scrollable}

<mark>***Revisiting the conditions for coexistence***</mark>

- Each species exerts stronger competitive effects on conspecifics than on heterspecifics
- *Heuristic*: neither species can be substantially more sensitive to competition than others

:::{.fragment}
<mark> What does this require? </mark>

- Species have separate niches
  - competition should be stronger within species than between species
- Species have relatively equivalent fitness in the system
:::

##

<mark>***Zooming back out***</mark>

![](img/network.png)


## 

***How can we expand to include more ecological complexity?***


![](img/network.png)

## {.scrollable}

<mark>***How can we expand to include more ecological complexity?***</mark>

One approach: use the L-V competition framework to tackle more complex systems.

- Recall the definition of competition:
- **One species modifies the environment in a way that suppresses growth of another species** ($\uparrow N_1$ leads to  $\downarrow N_2$)
- In principle, this can also include indirect interactions
  - E.g. As plant species $1$ grows, it promotes the growth of a herbivore species
  - Growth of the herbivore suppresses not just Plant $1$, but also starts affecting other plants in the system

##


## {.scrollable}

<mark>***How can we expand to include more ecological complexity?***</mark>

One approach: use the L-V competition framework to tackle more complex systems.

- In principle, we can also extend this to include positive interactions
  - E.g. As plant species $1$ grows, it cultivates mutualistic fungi in the soil
  - As fungi accumulate, they promote growth of other plants in the system

##

![](img/bever.png)


## 
![](img/psf.png)


## 

<mark>***The insights from the Lotka-Volterra competition model apply broadly***</mark>

- In a system where plants modify the soil community (and vice versa), ***what is required for plant coexistence***? 

- Each species cultivates a soil community that benefits conspecifics less than it benefits heterospecifics^[or harms the conspecific more than the heterospecific]

## How can we test this empirically? 

![](img/psf-mterms.png)

## How can we test this empirically?
![](img/psf-expt-1.png){width=900 height=300}


## How can we test this empirically?
![](img/psf-expt-2.png){width=900 height=600}

## How can we test this empirically?

![](img/gsk-psf.png)

## How do microbes shape microbes in real communities 

- It is tempting to look for simple answers, but...
- The reality is that it depends. There is lots of evidence that microbes can favor plant diversity in some cases, or hinder plant diversity in other cases.

<br>

- What does it depend on?
  - Various things, including environmental conditions

## 

![](img/dudenhoeffer.png)

## 

![](https://upload.wikimedia.org/wikipedia/commons/8/84/Texas_bullnettle_%26_plains_coreopsis%2C_Attwater_Prairie_Chicken_National_Wildlife_Refuge%2C_Colorado_Co.%2C_TX%2C_USA_%283_May_2018%29.jpg)

## 

![](img/psf-expt-2.png){width=900 height=600}

<mark>***For 8 species, under three watering regimes - a HUGE task!***</mark>

##

:::: {.columns}

::: {.column width="33%"}
![](img/dudenhoeffer-result.png)
:::

::: {.column width="67%"}
- Most stabilziation (highest chance of coexistence) under low watering regime
- Least stabilization (least chance of coexistence) under high watering regime
::: 

::::

