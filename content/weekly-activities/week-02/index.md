---
date: "2021-01-01"
draft: false
type: collection
excerpt: Population Ecology, part 1
subtitle: ""
title: Week 02
weight: 2
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week2/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976689
- icon: book
  icon_pack: fa
  name: Weekly Assignment
  url: https://moodle.lsu.edu/mod/assign/view.php?id=2014061
---

## Overview

This week's lectures will provide an introduction to how ecologists model the process of populations growing or shrinking. We will begin with a simple model that considers a "closed"  population that only grows with births, and only shrinks with deaths. We will see how we can add biological complexity to these simple models, and how even simple constraints can give rise to complex patterns of population dynamics. 

For this week's activity, students are asked to read [this paper](/pdf/Miriti_JoE_2006.pdf) by Dr. Maria Miriti about facilitation and competition in a population of *Artemesia dumosa*. After reading the paper, students are asked to answer the questions on [this activity sheet](/pdf/miriti-paper-questions.pdf).

Thanks to [Cedric Letsch](https://unsplash.com/@cedricletsch) for providing the header photo on [Unsplash](https://unsplash.com/photos/FYDNbPE9Bw4)