---
date: "2021-01-01"
draft: false
type: collection
excerpt: Semester retrospective
subtitle: ""
title: Week 15
weight: 15
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week15/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976862
- icon: pagelines
  icon_pack: fab
  name: Weekly activity
  url: https://moodle.lsu.edu/mod/assign/view.php?id=2112300
---

## Overview

This week, we will do a retrospective of the semester to summarize what we have learned, how these topics relate to challenges touching each of our lives, and how you can apply this knowledge moving forward.

Image of a reflective lake in Austria by by [Christian Holzinger](https://unsplash.com/@pixelatelier?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/photography-of-trees-and-body-of-water-under-white-sky-during-daytime-bVOmGEOnEzc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)