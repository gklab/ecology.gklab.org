---
date: "2021-01-01"
draft: false
type: collection
excerpt: Ecosystem ecology, part 2
subtitle: ""
title: Week 13
weight: 13
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week13/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976856
---

## Overview

On Monday, we will break out into small groups to discuss the semester project updates. On Wednesday and Friday, we will continue the Ecosystem Ecology module by turning our attention to the Nitrogen cycle. 

Cover photo of the rhizobial nodules on *Vicia* roots from [Wikimedia Commons](https://en.wikipedia.org/wiki/Fabaceae#/media/File:Vicia_sepium10_ies.jpg)

