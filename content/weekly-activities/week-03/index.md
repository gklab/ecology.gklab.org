---
date: "2021-01-01"
draft: false
type: collection
excerpt: Population Ecology, part 2
subtitle: ""
title: Week 03
weight: 2
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week3/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976690
- icon: book
  icon_pack: fa
  name: Weekly Assignment
  url:  https://moodle.lsu.edu/mod/assign/view.php?id=2021909
---

## Overview

In lecture this week, we will learn how to incorporate in biological reality in the form of age or stage-structure in populations. For example, some populations may have many individuals, but if most individuals are very old and have low reproductive output, the population may nevertheless decline to extinction in the long-term. On the other hand, small populations may eventually grow to be big if they mostly comprise individuals with high survival rates and high fecundity (reproductive rates). 


For this week's activity, students are asked to read [this paper](/pdf/Crouse_Ecology_1987.pdf) by Dr. Deborah Crouse and colleagues about using stage-structured population modeling to develop effective conservation strategies for loggerhead sea turtles. After reading the paper, students are asked to answer the questions on [this activity sheet](/pdf/Week-3-activity-questions.pdf).

Cover image from [Wikimedia](https://en.wikipedia.org/wiki/Loggerhead_sea_turtle).