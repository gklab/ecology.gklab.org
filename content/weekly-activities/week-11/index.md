---
date: "2021-01-01"
draft: false
type: collection
excerpt: Patterns of biodiversity
subtitle: ""
title: Week 11
weight: 11
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week11/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976856
---

## Overview

This week, we will learn about the theory of Island Biogeography, a framework originally developed to understand distrubition of biodiversity on islands, but which has since had a huge impact on conservation.

Cover photo of [Kai Bae View Point, Ko Chang, Thailand](https://unsplash.com/photos/tree-covered-islands-during-daytime-aerial-photo-H0RGAEEwzNg) by  [Ragnar Vorel](https://unsplash.com/@sonuba) on Unsplash.