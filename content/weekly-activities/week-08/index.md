---
date: "2021-01-01"
draft: false
type: collection
excerpt: Community ecology, Part 4
subtitle: ""
title: Week 08
weight: 8
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week8/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976854
- icon: book
  icon_pack: fa
  name: Weekly Assignment
  url:  
- icon: hippo
  icon_pack: fa
  name: Semester Project - Annotated Bibliograpy
  url:  /pdf/annotated-bibliography.pdf
- icon: hippo
  icon_pack: fa
  name: In-class activity
  url:  /pdf/Week-8-inclass-activity.pdf
---

## Overview

This week, we will learn about the role that consumer-resource interactions can have on ecological communities. 


The Semester Project Annotated Bibliography assignment is also due this week. 



Cover photo by [Avel Chuklanov](https://unsplash.com/@chuklanov) on [Unsplash](https://unsplash.com/photos/HTyONr8k5TQ)