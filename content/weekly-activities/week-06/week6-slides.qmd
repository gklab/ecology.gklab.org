---
title: "Community Ecology, part 2"
subtitle: "Principles of Ecology\nWeek 6"
format: 
  revealjs:
    theme: [moon, ../../../static/custom.scss]
    incremental: true   
    hash-type: number
    output-file: "slides"
    chalkboard: true
    fig-width: 6
from: markdown+emoji
---

```{r setup, include = FALSE}
library(tidyverse)
library(ecoevoapps)
knitr::opts_chunk$set(echo = FALSE, fig.align = 'center')
```

## Community ecology 

![](img/network.png)

## 


##

**Lotka-Volterra Competition Model**

$$\frac{dN_1}{dT} = r_1N_1(1-\alpha_{11}N_1 - \alpha_{12}N_2)$$

$$\frac{dN_2}{dT} = r_2N_2(1-\alpha_{21}N_1 - \alpha_{22}N_2)$$

## 


![](http://uploads.edubilla.com/inventions/84/3a/competitiveexclusion2.png){width="800" fig-align="center"}

##

**Lotka-Volterra Competition Model**

$$\frac{dN_1}{dT} = r_1N_1(1-\alpha_{11}N_1 - \alpha_{12}N_2)$$

$$\frac{dN_2}{dT} = r_2N_2(1-\alpha_{21}N_1 - \alpha_{22}N_2)$$

<mark>***Under what conditions do both species co-exist?***</mark>

:::{.fragment}
**Signature of stable coexistence**:

- System is at equilibrium <br>
- Both species are present (abundance > 0)
:::

## 

<mark>***Under what conditions do both species co-exist?***</mark>


Approach: **Graphical analysis of the competition model** 

- New type of visualization: <mark>***phase space***</mark> (AKA "state space") 
- New type of analysis: <mark>***null-cline analysis***</mark><br>(AKA "zero net-growth isocline") 

## 

**Defining the phase space (AKA state space)** 

- Set of all possible states in a dynamic system 
- In our context, all the possible values of $N_1$ and $N_2$ (i.e. all possible values of *state variables*)

## 

**Visualizing the phase (state) space**

- Graph with each axis representing a state variable

- Any point on the graph represents a possible state of the system

- Lines on the graph show how a system changes through time (***trajectory***)


## 

![](img/IMG_4985.jpg)

<!--Draw a phase space-->
<!--Label the axes-->
<!--Show the trajectory-->

## 

**Defining null-cline analyses (AKA zero net growth isocline analysis)**

- At what points in the state space does the system not change? (i.e. is at equilibrium) 
- Analyzed one axis at at time

- ***Key questions:*** <br>At what points does the abundance of species 1 ($N_1$) not change? <br>
At what points does the abundance of species 2 ($N_2$) not change? 

## {.scrollable}

<mark>***At what points does the abundance of species 1 (N1) not change?***</mark>

:::{.fragment} 

Two "extreme" cases...

:::

- When species 1 is at its carrying capacity, and species 2 is not around
  - $N_1 = \frac{1}{\alpha_{11}}, N_2 = 0$
  
- When there are so many individuals of Species 2 that Species 1 cannot begin to grow
  - When does that happen? 
  - $N_1$ is low (0), and $N_2$ is... some high number

##


$$\frac{dN_1}{dT} = r_1N_1(1-\alpha_{11}N_1 - \alpha_{12}N_2)$$

:::{.fragment}
Solve for $\frac{dN_1}{dT} = 0, N_1 = 0, N_2 > 0$
:::

## 

<mark>***At what points does the abundance of species 1 (N1) not change?***</mark>

::: {.nonincremental}
- When species 2 is absent, and species 1 is at its carrying capacity
  - $N_2 = 0, N_1 = \frac{1}{\alpha_{11}}$
  
- When there are so many individuals of Species 2 that Species 1 cannot begin to grow
  - When does that happen? 
  - $N_1$ is low, and $N_2$ is... some high number
::: 

  - $N_1 = 0, N_2 = \frac{1}{\alpha_{12}}$

## {.scrollable}

**Summary of the null-cline analysis so far:**

- We set out to find cases where $N_1$ doesn't change, i.e. $dN_1/dt = 0$
- We identified two extreme scenarios:
  - $N_1 = 1/\alpha_{11}, N_2 = 0$
  - $N_1 = 0, N_2 = 1/\alpha_{12}$

- We can put these two extreme points on the state space graph. 

##
  
![](img/IMG_4986.jpg)

## 

:::{.fragment}
<mark>***Growth of species 1 is also zero for intermediate combinations between these extremes***</mark>
:::

:::{.fragment}
$$\frac{dN_1}{dT} = r_1N_1(1-\alpha_{11}N_1 - \alpha_{12}N_2)$$
:::

:::{.fragment .fade-in-then-out}
$$N_2^* = \frac{1-\alpha_{11}N1}{\alpha_{12}} $$
:::

:::{.fragment}
$$N_2^* = \frac{1-\alpha_{11}N1}{\alpha_{12}} = \frac{1}{\alpha_{12}} + \frac{\alpha_{11}}{\alpha_{12}}N_1$$
:::

##


<mark>***Growth of species 1 is also zero for intermediate combinations between these extremes***</mark>

$$\frac{dN_1}{dT} = r_1N_1(1-\alpha_{11}N_1 - \alpha_{12}N_2)$$

$$N_2^* = \overbrace{\frac{1}{\alpha_{12}}}^{\text{y-intercept}} + \overbrace{\frac{\alpha_{11}}{\alpha_{12}}}^{\text{slope}}N_1$$

:::{.fragment}
<mark>***This is the equation of the null-cline for species 1*** (AKA zero net-growth isocline, or ZNGI)</mark>
:::

## 

![](img/IMG_4987.jpg)

##

What happens on either side of the null-cline?

![](img/IMG_4988.jpg)

## 

**Recall the key questions of null-cline analysis:**

- ***Key questions:*** <br>At what points does the abundance of species 1 ($N_1$) not change? :ear_of_rice:
  - $N_1 = 1/\alpha_{11}, N_2 = 0$; <br> $N_1 = 0, N_2 = 1/\alpha_{12}$; <br> $N_2^ = \frac{1}{\alpha_{12}} + \frac{\alpha_{11}}{\alpha_{12}}N_1$

:::{.fragment}
-----------
:::

- At what points does the abundance of species 2 ($N_2$) not change? 

## 

<mark>***At what points does the abundance of species 2 (N<sub>2</sub>) not change?***</mark>


- When species 1 is not around, and species 2 is at its carrying capacity
  - $N_1 = 0, N_2 = 1/\alpha_{22}$

- When there are so many individuals of Species 1 that Species 2 cannot begin to grow 
  - $N_1$ is some high number... and $N_2$ is low (0)
  - Following algebra, $N_1 = 1/\alpha_{21}, N_2 = 0$

- We can add these two extremes to the state space plot


## 

<mark>***Growth of species 2 is also zero for intermediate combinations between these extremes***</mark>

:::{.fragment}
$$\frac{dN_2}{dT} = r_2N_2(1-\alpha_{21}N_1 - \alpha_{22}N_2)$$
::: 

:::{.fragment}
$$N_2^* = \frac{1}{\alpha_{22}} - \frac{\alpha_{21}}{\alpha_{22}}N_1$$
:::


:::{.fragment}
<mark>***This is the equation of the null-cline for species 2*** (AKA zero net-growth isocline, or ZNGI)</mark>
:::

## 

Add null-cline to state space

![](img/IMG_4989.jpg)



## 

What happens on either side of the nullcline?
![](img/IMG_4990.jpg)



## {.scrollable}



<mark>***These possible outcomes are the focus of this week's Activity***</mark>

## Next class

- More time on Null-Cline analysis
- How this analysis helps us understand coexistence

# Principles of Ecology <br>Week 6, Day 2: Community ecology, con't

## Reminders {.scrollable}

- Please return to previous Weekly Reflections and self-assign a grade, if you have not done so already. (Leave grade as a comment on Moodle)

- For self-grading Weekly Activities, please compare your submissions to the Sample Responses sheet that I provide. 
  - You don't need to have the same answers as mine to give yourself full credit
  - But, if you were off track, please make sure you reflect on the gaps 

- For this week:
  - Weekly Activity due on Moodle before class on Friday (2pm)
  - Weekly Submission due on Sunday 

  
## 

<iframe width="1000" height="600" src="https://www.youtube.com/embed/vewtmQ5xrtU?si=8c_ACblJN47VeZLP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## 

**Graphical analysis of the Lotka-Volterra competition model**  

Review: 

- Our goal is to identify conditions under which both species can coexist at equilibrium
- Being 'at equilibrium' means $dN_1/dt = dN_2/dt = 0$
- Null-cline analysis lets us find the conditions at which $dN_1/dt = 0$, and the conditions at which $dN_2/dt$
- There are a couple of 'extreme' cases (e.g. one species at carrying capacity, and the other absent), and a whole bunch of in-between cases that result in $dN_1/dt = 0$ or $dN_2/dt = 0$

## 

**Test your recollection**

- Consider a pair of species that interact with the following strength:
  - $\alpha_{11} = 0.01$, $\alpha_{12} = 0.005$, $\alpha_{22} = 0.02$, $\alpha_{21} = 0.001$
  - ($\frac{1}{0.01} = 100,\ \frac{1}{0.01} = 200,\ \frac{1}{0.02} = 50,\ \frac{1}{0.001} = 1000$)
- On separate graphs, draw the isoclines for species 1 and 2. 


# Community Ecology, part 2 <br>Day 3



## {.scrollable}

**Worked examples**

- Case 1: 
  - $\frac{1}{\alpha_{11}} = 1000,\ \frac{1}{\alpha_{12}} = 1200,\ \frac{1}{\alpha_{22}} = 600,\ \frac{1}{\alpha_{21}} = 2000$

:::{.fragment}
![](img/IMG_4992.jpg)
:::

:::{.fragment}
![](img/IMG_4993.jpg)
:::

## {.scrollable}


- Case 2: 
  - $\frac{1}{\alpha_{11}} = 1500,\ \frac{1}{\alpha_{12}} = 900,\ \frac{1}{\alpha_{22}} = 600,\ \frac{1}{\alpha_{21}} = 750$ 
  
:::{.fragment}
![](img/IMG_4994.jpg)
:::

:::{.fragment}
![](img/IMG_4995.jpg)
:::

## 

- Case 3: 
  - $\frac{1}{\alpha_{11}} = 800,\ \frac{1}{\alpha_{12}} = 1000,\ \frac{1}{\alpha_{22}} = 1100,\ \frac{1}{\alpha_{21}} = 1200$



## 

- Case 4: 
  - $\frac{1}{\alpha_{11}} = 1500,\ \frac{1}{\alpha_{12}} = 1000,\ \frac{1}{\alpha_{22}} = 1800,\ \frac{1}{\alpha_{21}} = 1200$
  
## 

<mark>***Based on these four case studies, and on your activity, what needs to be true for species to coexist?***</mark>


##
**Possible outcomes of Lotka-Volterra competition** 

- Both species coexist
- *Priority effects*: Initially dominant species persists
- Species 1 persists; species 2 outcompeted
- Species 2 persists; species 1 outcompeted

## {.scrollable}


::: {layout="[[1,1], [2,2]]"}
![](img/IMG_4993.jpg)

![](img/IMG_4995.jpg)
![](img/IMG_4998.jpg)
![](img/IMG_4999.jpg)
:::

## 

## 

<mark>***What are the general conditions for coexistence?***</mark>

- Stronger within-species interactions (intra-specific competition) than between-species interactions (inter-specific competition)
  - When does this happen?
  - Each species has slightly distinct niches

- Neither species is substantially more affected by competition than the other species
  - When does this happen?
  - Relatively similar in terms of average fitness

## {.scrollable}

<mark>***Possible outcomes in null-cline analysis***</mark>


- Each species experiences intense intra-specific competition, but weak inter-specific competition
  - $\uparrow \alpha_{11}, \alpha_{22}$, and $\downarrow \alpha_{12}, \alpha_{21}$

- Each species experiences weak intra-specific competition, but intense inter-specific competition
  - $\downarrow \alpha_{11}, \alpha_{22}$, and $\uparrow \alpha_{12}, \alpha_{21}$

- One species experiences intense intra- and inter-specific competition, the other does not
  - $\downarrow \alpha_{11}, \alpha_{12}$, and $\uparrow \alpha_{22}, \alpha_{21}$ <br> *Species 1 isn't as affected by competition as Species 2 is*     
  - $\uparrow \alpha_{11}, \alpha_{12}$, and $\downarrow \alpha_{22}, \alpha_{21}$ <br> *Species 1 is more strongly affected by competition than Species 2*


##

<iframe width="1000" height="600" src="https://www.youtube.com/embed/yYXoCHLqr4o?si=8kHw59UFUDjNHkI_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>