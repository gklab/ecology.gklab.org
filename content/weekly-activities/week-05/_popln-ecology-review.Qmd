---
title: ""
format: 
  pdf:
    output-file: "Week-5-activity-questions"
geometry: bmargin=2cm
---

```{r, setup, include = FALSE}
library(tidyverse)
knitr::opts_chunk$set(echo = FALSE, fig.height = 2.5, fig.width = 3, fig.align='center',fig.pos='H')
empty_graph <- 
  ggplot() + 
  geom_hline(yintercept = 0) + 
  geom_vline(xintercept = 0) +
  theme_void() + 
  xlim(0,5) + ylim(0,5)


library(diagram)
plot_leslie_diagram <- function(leslie_mat, names_given = NULL) {
  n_ages <- nrow(leslie_mat)
  if(is.null(names_given)) {
    name_vec <- parse(text = paste0("Age[",0:(n_ages-1),"]"))
  } else{
    name_vec <- names_given
  }
  diagr <- plotmat(A = leslie_mat, pos = n_ages,
                            curve = 0.5, lwd = 1.5, my = -0.1,
               name = name_vec,
               arr.length = 0.2, arr.width = 0.25, arr.lwd = 2,
               arr.type = "simple", self.lwd = 2, self.shiftx = 0.115,
               self.shifty = 0.1, self.cex = .5, box.size = 0.1,
               dtext = 0.2, box.lwd = 3.5, main="", mar = c(0,0,0,0)
               )

  return(diagr)

}

```


## Population ecology roundup

Please answer the following questions about population dynamics. You might find this resource helpful while answering the questions: https://ecoevoapps.shinyapps.io/population_growth/

1.  Consider two populations growing exponentially. One grows at a rate of $r_1 = 0.25$, the other, at a rate of $r_2 = 0.3$. Assuming they both start with an initial size of $N = 10$, sketch the trajectory (**shape**) of their growths on the following graph. *Please label both axes, and indicate the value of the y-intercept*. *(0.5 points)*

```{r}
empty_graph
```


2. Consider a population experiencing logistic growth, with $r = 0.7$ and $K = 400$.  Sketch the trajectory of its growth assuming an initial size of $N = 10$. *Please label both axes, and indicate the value of the y-intercept and indicate the carrying capacity*. *(0.5 points)*

```{r}
empty_graph
```

\clearpage

3. Consider another species experiencing logistic growth, this time with a time-lag. As above, the intrinsic growth rate is $r = 0.7$, and $K = 400$ - but for this species, there is a substantial time-lag of $\tau = 2$.  *Please label both axes, and indicate the value of the y-intercept and indicate the carrying capacity*. *(0.5 points)*

```{r}
empty_graph
```

4. While the logistic model of population growth only considers intra-specific competition, we also know that some forms of facilitation are common in nature (e.g. through the nurse effect, or when groups of animals cooperate to hunt in packs). How can we modify the logistic growth model to acccount for such facilitation that happens at low densities of $N$? What happens when a population reaches a size below the threshold value $T$?  *(0.5 points)*

\begin{Form}
\TextField[width=\linewidth, height=20em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}

\clearpage

5a. Write the transition matrix for an annual plant species whose biology is described by the following life cycle diagram. *(0.5 points)*

\vspace{-7em}

```{r, fig.width = 6, fig.height = 3.75}
leslie <- matrix(c(0, 0,30,
                   0.05,0,0,
                   0,0.25,0), byrow = T, nrow = 3)
tt <- plot_leslie_diagram(leslie, names_given = c("seed", "seedling", "adult plant"))
```

\vspace{-7em}

$$
\begin{bmatrix}
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\ 
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ \\
 ~ & ~ & ~ & ~ & ~ & ~ & ~ & ~& ~ & ~ 
\end{bmatrix} 
$$

\vspace{3em}

5b. Using just this matrix, how could you predict whether or not the species will persist on the landscape in the long-term (i.e. whether the species has positive or negative population growth)? *Note: you don't need to actually calculate the growth rate, simply explain what property of the matrix can give you this information* *(0.5 points)*

\vspace{3em}

5c. If this species' population were to become endangered, how can you use this matrix to identify the best stage to focus on when developing a conservation strategy? *(0.5 points)*


\clearpage

6. Draw a life cycle diagram for a species whose biology is described by the following transition matrix. *(0.5 points)*

$$
\begin{bmatrix}
0 & 2 & 30 & 10  & 0 \\
0.33 & 0 & 0 & 0 & 0  \\
0 & 0.7 & 0 & 0  & 0 \\ 
0 & 0 & 0.2 & 0  & 0 \\
0 & 0 & 0 & 0.5 & 0 
\end{bmatrix}
$$
\vspace{15em} 

7. Consider a species whose dynamics follow the meta-population dynamics model that we discussed in class.  
Due to recent habitat development, the connections between the species' habitats have been severely degraded such that the migration rate $m = 0.05$. What is the maximum rate of local extinction ($e$) that the species can withstand to prevent landscape extinction? *(0.5 points)*

\begin{Form}
\TextField[width=\linewidth, height=15em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}

\clearpage 

8. What is one theme/idea from the Population Ecology unit that you wish we spent more time covering - either in its mathematical formulation, or in empirical applications? [Feel free to name more than 1 if you wish.] *(0.5 points)*

\begin{Form}
\TextField[width=\linewidth, height=15em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}