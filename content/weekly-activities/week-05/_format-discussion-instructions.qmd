---
title: ""
format: 
  pdf:
    output-file: "semester-project-fromat-discussion"
geometry: margin=1.5cm
---

## Structured group discussion

- Groups of 4 people each, discussion for ~25 minutes.
- Nominate one person as *facilitator*, one as *recorder*, one as *reporter*


In your groups, please discuss some of the following questions:

- What idea(s) for potential you had going into this assignment, and why?

- After having thought about the potential format(s) and how their relation to your proposed ecological community, do you think your proposed format offers enough room to convey the community's ecology and environmental challenges?

- Have you worked in your proposed format before? If so, what do you want to do this time around that challenges you to develop new creative skills? If not, what are some aspects of this medium that you are most nervous about?

- What are some examples of other projects in this format that you find inspiring? What do you think makes it special/inspiring?

- What was one paper/idea/resource that surprised you, or that you think will be helfpul? 

- What are some steps you want to start taking to connect your proposed ecological community to your proposed format?

---------------------

## Structured group discussion

- Groups of 4 people each, discussion for ~25 minutes.
- Nominate one person as *facilitator*, one as *recorder*, one as *reporter*

In your groups, please discuss some of the following questions:

- What idea(s) for potential you had going into this assignment, and why?

- After having thought about the potential format(s) and how their relation to your proposed ecological community, do you think your proposed format offers enough room to convey the community's ecology and environmental challenges?

- Have you worked in your proposed format before? If so, what do you want to do this time around that challenges you to develop new creative skills? If not, what are some aspects of this medium that you are most nervous about?

- What are some examples of other projects in this format that you find inspiring? What do you think makes it special/inspiring?

- What was one paper/idea/resource that surprised you, or that you think will be helfpul? 

- What are some steps you want to start taking to connect your proposed ecological community to your proposed format?

