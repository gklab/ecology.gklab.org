---
date: "2021-01-01"
draft: false
type: collection
excerpt: Community ecology, Part 1
subtitle: ""
title: Week 05
weight: 5
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week5/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976692
- icon: book
  icon_pack: fa
  name: Weekly Assignment
  url:  https://moodle.lsu.edu/mod/assign/view.php?id=2038186
- icon: hippo
  icon_pack: fa
  name: Semester Project update
  url:  https://moodle.lsu.edu/mod/assign/view.php?id=2038133
---

## Overview

In lecture this week, we will turn to the field of community ecology, which investigates the relationships between different species that co-occur in the same habitat. We will begin with an overview of the different types of reactions that can take place between organisms, and then delve into ways of studying interactions between species that occupy the same *guild*. 

The [Weekly Activity](/pdf/Week-5-activity-questions.pdf) is a detailed review of the material we have covered in class so far. You will likely have to go back to previous notes, and use outside resources, to answer questions about exponential and logistic population growth, transition matrices, metapopulation dynamics. 

We will also spend some time this week focused on the semester project - the goal for this week is to [explore potential formats/media](/pdf/potential-formats.pdf) that students can work in. We will end the week with in-class small group discussions about the [semester project formats](/pdf/semester-project-format-discussion.pdf)

Cover photo by <a href="https://unsplash.com/@pamelaheckel?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Pamela Heckel</a> on <a href="https://unsplash.com/photos/nGc7dxhp6IY?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  