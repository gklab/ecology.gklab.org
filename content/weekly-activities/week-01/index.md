---
date: "2021-01-01"
draft: false
type: collection
excerpt: What is ecology, and how do we do it?
subtitle: ""
title: Week 01
weight: 1
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week1/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1952086
- icon: binoculars
  icon_pack: fa
  name: Who's in class survey
  url: https://moodle.lsu.edu/mod/questionnaire/view.php?id=1952648
- icon: dice-d20
  icon_pack: fa
  name: Mark-Recapture activity
  url: pdf/mark-recapture-activity.pdf
---

## Overview

The goal for this week is introduce you to the Science of Ecology.

Monday's class will introduce students to the types of questions ecologists ask, the types of challenges we run into when searching for answers, and the ways in which ecologists address these challenges. 

On Wednesday, we will go over how the course is structured - what major topics we will cover, what your weekly responsibilities will be, and how grading will work for this course. We will also get a taste of some of the global biodiversity patterns and threats that puzzle and motivate ecologists. 

Friday's session will be built around an activity in which students work in small groups to answer a seemingly easy but surprisingly challenging ecological question. 