---
date: "2021-01-01"
draft: false
type: collection
excerpt: Mid-semester check-in
subtitle: ""
title: Week 09
weight: 9
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: slides/week9/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976855
- icon: book
  icon_pack: fa
  name: Weekly Assignment
  url: pdf/Week-9-activity-questions.pdf
---

## Overview



Cover photo by [Avel Chuklanov](https://unsplash.com/@chuklanov) on [Unsplash](https://unsplash.com/photos/HTyONr8k5TQ)