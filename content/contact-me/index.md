---
title: ''
author: "Gaurav Kandlikar"
show_post_date: no
layout: "single-sidebar"
type: "singlepage"
sidebar_left: yes
details: false
---

If you are a student in Biol 4253, please contact me through the course moodle page. You can also choose to email me (gkandlikar@lsu.edu), but my response times may be slower. 

If you are interested in making a fork of this website for your own course, please check out the website source code on [GitLab](https://gitlab.com/gklab/ecology.gklab.org). Feel free to make an Issue on gitlab or email me at gkandlikar@lsu.edu if you have any questions. Even if you don't have any questions, I would love to hear from you if you find this website useful!